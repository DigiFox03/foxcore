from abc import ABC, abstractmethod
import discord
from foxcore import permissions


class Feature(ABC):
    @abstractmethod
    def is_worth(self, *, client: discord.Client, message: discord.Message, data, **_):
        pass

    @abstractmethod
    def is_valid(self, *, client: discord.Client, message: discord.Message, data, **_):
        pass

    def is_permitted(self, *, client: discord.Client, message: discord.Message, data, **_):
        plist = data["user"]["permissions"].copy()
        if message.guild is not None:
            plist += data["user"]["permissions"]
        return permissions.check(plist, **self.permissions)

    def on_message(self, *, client: discord.Client, message: discord.Message, data, **_):
        pass
