from abc import ABC, abstractmethod


class PPStep(ABC):
    @abstractmethod
    def run(self, **_):
        pass
