from abc import ABC, abstractmethod


class Block(ABC):
    @abstractmethod
    async def input(self, **kwargs) -> dict:
        pass
