class Pipeline(object):
    def __init__(self):
        self.genertors = []
        self.blocks = []
        self.gens = []

    def append_block(self, block):
        self.blocks.append(block)

    def set_blocks(self, blocks: list):
        self.blocks = blocks

    async def run(self, data):
        data.update({"cache": []})
        for block in self.blocks:
            new_data = await block.input(**data)
            data.update(new_data)
