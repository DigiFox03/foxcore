import asyncio
import timeit
from foxcore.feature import Feature


class Ping(Feature):
    def __init__(self, prefix="fc-"):
        self.prefix = prefix
        self.permissions = {
            "pname": "ping",
            "default": True,
            "bot_min": 3,
            "server_min": 3,
            "enabler": ["debug"],
            "disabler": []
        }

    def is_worth(self, *, client, message, data, **_):
        if message.guild is not None:
            perm = message.guild.me.permissions_in(channel=message.channel)
            return perm.send_messages
        return True

    def is_valid(self, *, client, message, data, **_):
        content = message.content  # type: str
        return content.lower().startswith(self.prefix + "ping")

    async def on_message(self, *, client, message, data, **_):
        channel = message.channel  # type: discord.TextChannel

        def check(m):
            return m.content == "Pong!" and m.author == client.user

        start = timeit.default_timer()
        await asyncio.gather(
            channel.send(content="Pong!"),
            client.wait_for("message", check=check)
        )
        end = timeit.default_timer()
        tt = end - start
        await channel.send(f"took {tt} seconds (discord latency {client.latency})")
