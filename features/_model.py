from foxcore.feature import Feature


class FeatureName(Feature):
    def is_worth(self, *, client, message, data, **_):
        return False

    def is_valid(self, *, client, message, data, **_):
        return False

    def is_permitted(self, *, client, message, data, **_):
        return False

    async def on_message(self, *, client, message, data, **_):
        return False
