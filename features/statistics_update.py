import discord
from foxcore.feature import Feature


class StatsUpdate(Feature):
    def __init__(self, statistic, prefix="fc-"):
        self.prefix = prefix
        self.statistic = statistic
        self.permissions = {
            "pname": "stats_update",
            "default": False,
            "bot_min": 2,
            "server_min": 0,
            "enabler": [],
            "disabler": []
        }

    def is_worth(self, *, client, message, data, **_) -> bool:
        return True

    def is_valid(self, *, client, message, data, **_):
        content = message.content  # type: str
        return content.lower().startswith(self.prefix + "stats-update")

    def is_permitted(self, *, client, message, data, **_):
        return True

    async def on_message(self, *, client, message, data, **_):
        await self.statistic.update_database()
        try:
            await message.channel.send("pushed cache to db")
        except discord.HTTPException:
            pass
