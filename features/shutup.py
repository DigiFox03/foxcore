from foxcore.feature import Feature
from foxcore import permissions


class Shutup(Feature):
    def __init__(self, prefix="fc-"):
        self.prefix = prefix
        self.permissions = {
            "pname": "shutup",
            "default": False,
            "bot_min": 3,
            "server_min": 2,
            "enabler": ["emergence"],
            "disabler": []
        }

    def is_worth(self, *, client, message, data, **_):
        return True

    def is_valid(self, *, message, **_):
        return message.content.lower().startswith(self.prefix + "shutup")

    def is_permitted(self, *, data, message, **_):
        plist = data["user"]["permissions"].copy()
        if message.guild is not None:
            plist += data["user"]["permissions"]
        rt = permissions.check(plist, **self.permissions)
        if not rt and message.guild is not None:
            rt = message.channel.permissions_for(message.author).delete_messages
        return rt

    async def on_message(self):
        pass
