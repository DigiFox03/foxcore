import discord
from foxcore.feature import Feature


class ChannelSetting(Feature):
    def __init__(self, prefix):
        self.prefix = prefix
        self.com = self.prefix + "channel"
        self.help = (
            f"{self.com} type get\n"
            f"{self.com} type set <type>"
        )
        self.permissions = {
            "pname": "settings",
            "default": False,
            "bot_min": 3,
            "server_min": 2,
            "enabler": [],
            "disabler": []
        }

    def is_worth(self, *, client, message, data, **_):
        return True

    def is_valid(self, *, client, message, data, cache, **_):
        if message.author == client.user:
            return False
        if not message.content.lower().startswith(self.com):
            return False
        cache.append([p for p in message.content.lower().split(" ") if p][1:])
        return True

    async def on_message(self, *, client, message, data, cache, **_):
        args = cache.pop()
        ln = len(args)
        if ln < 2:
            mess = self.help
        elif args[0] == "type":
            if args[1] == "get":
                type_ = data["channel"]["type"]
                channel = data["channel"]["_id"]
                mess = f"<#{channel}> type is {type_}"
            elif args[1] == "set":
                if ln < 3:
                    mess = self.help
                else:
                    data["channel"]["type"] = args[2]
                    type_ = data["channel"]["type"]
                    channel = data["channel"]["_id"]
                    mess = f"<#{channel}> type is {type_} now"
            else:
                mess = self.help
        else:
            mess = self.help
        try:
            await message.channel.send(mess)
        except discord.Forbidden:
            pass
