from foxcore.feature import Feature


class PipelineDebug(Feature):
    def __init__(self, prefix="fc-"):
        self.prefix = prefix
        self.permissions = {
            "pname": "pipeline_debug",
            "default": False,
            "bot_min": 3,
            "server_min": 0,
            "enabler": ["debug"],
            "disabler": []
        }

    def is_worth(self, *, client, message, data, **_):
        if message.guild is not None:
            permissions = message.guild.me.permissions_in(channel=message.channel)
            return permissions.send_messages
        return True

    def is_valid(self, *, client, message, data, **_):
        content = message.content  # type: str
        return content.lower().startswith(self.prefix + "debug-pipeline")

    async def on_message(self, **kwargs) -> bool:
        message = kwargs["message"]
        channel = message.channel  # type: discord.TextChannel
        com = message.content.split(" ")
        if len(com) == 1:
            mess = str(kwargs.keys())
        else:
            try:
                curr = kwargs
                for plc in com[1:]:
                    if hasattr(curr, plc):
                        getattr(curr, plc)
                    else:
                        curr = curr[plc]
                mess = str(curr)
            except KeyError:
                mess = f"key error: {plc}"
        if len(mess) > 1800:
            mess = "message too long"
        await channel.send(content="`" + mess + "`")
