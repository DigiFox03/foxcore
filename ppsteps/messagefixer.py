import re
from ppstep import PPStep

BRACK = re.compile(r"^\(+|\)+$")


class MessageFixer(PPStep):
    def run(self, message, data, **_):
        if message.guild is not None:
            if data["channel"]["type"] == "roleplay":
                message.content = BRACK.sub("", message.content)
        return {"message": message}
