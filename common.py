import copy
import re

# splits "PascalCase" into "pascal case"
SPC = re.compile(r"((?<=[a-z0-9])[A-Z]|(?!^)[A-Z](?=[a-z]))")
# collapses spaces and initial and final bracket
WSREM = re.compile(r"(?<=[^-'a-zA-Z]) +| +(?=[^-'a-zA-Z])|^[]})><({[]+|[]})><({[]+$")
# splits into sequences of elemens of the [-'a-zA-Z] group or the opposite one
# "hello12!" -> ["hello", "12!"]
SPLITFIND = re.compile(r"(?<![^-'a-zA-Z])(?=[^-'a-zA-Z])|(?<=[^-'a-zA-Z])(?![^-'a-zA-Z])")
SPLIT = re.compile(r" ")
ISNAME = re.compile(r"[-'a-zA-Z]+")  # checks if it is a valid name
BRAKOPEN = re.compile(r"[<({[]")  # open bracket
BRAKCLOSE = re.compile(r"[]})>]")  # closed bracket


def dict_hash(o):
    if isinstance(o, (set, tuple, list)):
        return tuple([dict_hash(e) for e in o])
    elif not isinstance(o, dict):
        return hash(o)
    new_o = copy.deepcopy(o)
    for k, v in new_o.items():
        new_o[k] = dict_hash(v)
    return hash(tuple(frozenset(sorted(new_o.items()))))


def nickname_dec(*v_name, lenght_hint=6, max_lenght_hint=12, preferency=5):
    names = []
    for num, name in enumerate(v_name):
        braket = 0
        name = WSREM.sub(" ", SPC.sub(r" \1", name))
        tokens = SPLIT.split(SPLITFIND.sub(" ", name))
        for token in tokens:
            if ISNAME.match(token):
                names.append((
                    - abs(len(token) - lenght_hint)
                    - braket * max_lenght_hint
                    - num * preferency,
                    token
                    ))
            else:
                braket += (
                    + len(BRAKOPEN.findall(token))
                    - len(BRAKCLOSE.findall(token))
                    )
    names.sort(reverse=True)
    if names:
        return names[0][1][:12]
    return None
