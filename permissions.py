class PermError(object):
    def __init__(self, pset):
        self.pset = pset

    def error(self, error, **kwargs):
        try:
            msg = "{code}: {msg}".format(
                msg=self.pset[error]["desc"],
                code=self.pset[error]["code"]
            )
        except KeyError:
            msg = "Unknow error"
        try:
            return msg.format(**kwargs)
        except KeyError:
            return msg


ALIAS = {
    "root": (1, 1),
    "admin": (1, 2),
    "mod": (1, 3),
    "ser-root": (2, 1),
    "ser-admin": (2, 2),
    "ser-mod": (2, 3)
}


def check(
        plist, pname,
        default=True, bot_min=3, server_min=3,
        enabler=None, disabler=None
):
    if enabler is None:
        enabler = []
    if disabler is None:
        disabler = []
    enabler.append(pname)
    disabler.append("-" + pname)
    state = False
    for element in plist:
        if element == "default" and not state:
            state = default
        if element in ALIAS and not state:
            tp = ALIAS[element][0]
            v = ALIAS[element][1]
            state = (tp == 1 and v <= bot_min) or (tp == 2 and v <= server_min)
        elif element in enabler:
            state = True
        elif element in disabler:
            state = False
    return state
