import re
import logging
from foxcore.pipeline import Block

log = logging.getLogger(__name__)


class ChannelPerm(Block):
    def __init__(self, prefix):
        self.ooc = re.compile(r"^\(+|\)+$|^" + prefix)

    async def input(self, *, message, data, **_):
        if message.guild is None:
            perm = True
        else:
            log.debug("channel type: %s", data["channel"]["type"])
            if data["channel"]["type"] == "roleplay":
                perm = False
                if self.ooc.search(message.content) is not None:
                    perm = True
            elif data["channel"]["type"] == "reserved":
                perm = False
            elif data["channel"]["type"] == "default":
                perm = True
            else:
                log.warning(
                    "weird channel type : %s in %s at %s",
                    data["channel"]["type"],
                    message.guild,
                    message.channel
                )
                perm = False
        return {"perm": perm}
