from collections import defaultdict
import struct
from discord.utils import snowflake_time
from foxcore.pipeline import Block


class Activity(Block):
    def __init__(self):
        self.mem_act = defaultdict(float)
        self.usr_act = defaultdict(float)

    async def input(self, *, message, **_):
        ret = {}
        time = snowflake_time(message.id).timestamp()
        user = message.author.id
        old_user_act = self.usr_act[user]
        if "activity:time-reset" in message.content:
            time = 0
            del self.usr_act[user]
        if time > old_user_act:
            self.usr_act[user] = time
            ret.update({
                "last_user_act": old_user_act,
                "rel_user_act": time - old_user_act,
            })
        if message.guild is not None:
            memb = struct.pack("!QQ", message.guild.id, message.author.id)
            if "activity:time-reset" in message.content:
                del self.mem_act[memb]
            old_memb_act = self.mem_act[memb]
            if time > old_memb_act:
                self.mem_act[memb] = time
                ret.update({
                    "last_memb_act": old_memb_act,
                    "rel_memb_act": time - old_memb_act,
                })
        return ret
