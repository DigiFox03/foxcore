from collections import defaultdict
import asyncio
import struct
from copy import deepcopy
from pymongo import UpdateOne
from discord.utils import snowflake_time
from foxcore.pipeline import Block


class Statistics(Block):
    def __init__(self, db, sleep_time=10):
        def default():
            return defaultdict(
                lambda: {
                    "mess": [0] * 24 * 7,
                    "coms": defaultdict(int)
                    })
        self.data_default = {
            "user": default(),
            "member": default(),
            "guild": default(),
            "channel": default()
        }
        self.data = deepcopy(self.data_default)
        self.colls = {
            "user": db.coll_user,
            "member": db.coll_memb,
            "guild": db.coll_guil,
            "channel": db.coll_chan
            }
        self.sleep_time = sleep_time
        self.updater = None

    async def input(self, *, message, com=None, **_):
        if self.updater is None or self.updater.done():
            self.updater = asyncio.ensure_future(
                self.update_database(delay=self.sleep_time)
                )
        time = snowflake_time(message.id)
        wh = time.weekday() * 24 + time.hour
        if message.guild is not None:
            parts = {
                "user": message.author.id,
                "member": struct.pack("!QQ", message.guild.id, message.author.id),
                "guild": message.guild.id,
                "channel": message.channel.id
                }
        else:
            parts = {
                "user": message.author.id,
                }
        for key, value in parts.items():
            self.data[key][value]["mess"][wh] += 1
            if com is not None:
                self.data[key][value]["coms"][com] += 1
        return {}

    async def update_database(self, delay=0):
        await asyncio.sleep(delay)
        dt = self.data
        self.data = deepcopy(self.data_default)
        for collname, sect in dt.items():
            coll = self.colls[collname]
            batch = []
            for dname, document in sect.items():
                update = self.update_document(document)
                batch.append(
                    UpdateOne(
                        {"_id": dname, "mess": {"$exists": False}},
                        {"$set": {"mess": [0] * 24 * 7}}
                    )
                )
                batch.append(UpdateOne({"_id": dname}, update))
            if batch:
                asyncio.ensure_future(coll.bulk_write(batch))

    @staticmethod
    def update_document(doc):
        op = {"$inc": {}}
        for i, elm in enumerate(doc["mess"]):
            op["$inc"].update({
                f"mess.{i}": elm
            })
        for key, elm in doc["coms"].items():
            op["$inc"].update({
                f"coms.{key}": elm
            })
        return op
