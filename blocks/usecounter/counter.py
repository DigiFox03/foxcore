import logging
log = logging.getLogger(__name__)


class Counter(object):
    def __init__(self, logger=log):
        self._counter = 0
        self._id = 0
        self.log = logger

    def start_job(self):
        self._id += 1
        self._counter += 1
        self.log.info("started job %d, active jobs %d", self._id, self._counter)
        return self._id

    def end_job(self, jid=None):
        self._counter -= 1
        if id is None:
            self.log.info("finished job, active jobs %d", self._counter)
        else:
            self.log.info("finished job %d, active jobs %d", jid, self._counter)
