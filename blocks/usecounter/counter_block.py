from foxcore.pipeline import Block


class CounterBlock(Block):
    def __init__(self, counter, optype):
        self.counter = counter
        if optype not in ["enter", "exit"]:
            raise ValueError(f"optype has to be one of {['enter', 'exit']}")
        self.optype = optype

    async def input(self, job=None, **_):
        if self.optype == "enter":
            return {"job": self.counter.start_job()}
        if self.optype == "exit":
            self.counter.end_job(job)
            return {"job": None}
        return {}
