from logging import getLogger
from pprint import PrettyPrinter
from foxcore.pipeline import Block

log = getLogger(__name__)
fmtr = PrettyPrinter(indent=2, width=100, compact=True)


class InfoPrinter(Block):
    def __init__(self, logger=log, fmt=fmtr):
        self.log = logger
        self.fmt = fmtr

    async def input(self, job, message, data, **_):
        self.log.info(
            "job: %s\n%s",
            job,
            self.fmt.pformat(
                {
                    "mesage":
                    {
                        "guild": message.guild,
                        "channel": message.channel,
                        "content": message.content,
                        "author": str(message.author),
                        "author-bot": message.author.bot,
                        "author-id": message.author.id,
                        "id": message.id,
                    },
                    "data": data
                }
            )
        )
        return {}
