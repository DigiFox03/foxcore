import typing
from foxcore.permissions import PermError
from foxcore.pipeline import Block
from foxcore.feature import Feature


class Handler(Block):
    def __init__(self, feats: typing.List[Feature], pset):
        self.features = feats
        self.perm_error = PermError(pset)

    async def input(self, message, data, perm=True, **kwargs):
        if not perm:
            return {}
        kwargs.update({"message": message, "data": data, "perm": perm})
        for feature in self.features:
            if not hasattr(feature, "on_message"):
                continue
            if not feature.is_worth(**kwargs):
                continue
            if not feature.is_valid(**kwargs):
                continue
            p = feature.is_permitted(**kwargs)
            if _get(p, 0, p):
                kwargs["com"] = feature.__class__.__name__
                await feature.on_message(**kwargs)
            else:
                err = self.perm_error.error(_get(p, 1, "forbidden"))
                await kwargs["message"].channel.send(err)
            break
        return kwargs


def _get(obj, key, default):
    try:
        return obj[key]
    except (KeyError, TypeError):
        return default
