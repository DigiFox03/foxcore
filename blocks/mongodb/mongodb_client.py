import motor.motor_asyncio as mongodb


class ClientMongo(object):
    def __init__(
            self, uri="",
            database="db",
            users="users",
            members="members",
            channels="channels",
            guilds="guilds"
    ):
        self.uri = uri
        self.database_name = database
        self.users_name = users
        self.channels_name = channels
        self.guilds_name = guilds
        self.client = mongodb.AsyncIOMotorClient()
        self.database = self.client.get_database(name=database)
        self.coll_user = self.database.get_collection(users)
        self.coll_memb = self.database.get_collection(members)
        self.coll_chan = self.database.get_collection(channels)
        self.coll_guil = self.database.get_collection(guilds)
