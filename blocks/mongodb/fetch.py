import asyncio
import struct
from bson import Int64
from foxcore.pipeline import Block
from foxcore.common import dict_hash
from .mongodb_client import ClientMongo


class Fetcher(Block):
    def __init__(self, mdb: ClientMongo):
        self.mdb = mdb

    async def input(self, message, **kwargs):
        if message.guild is not None:
            data = await _get(
                self.mdb,
                message.author.id,
                message.guild.id,
                message.channel.id,
                )
        else:
            data = await _get(
                self.mdb,
                message.author.id
            )
        data_hash = dict_hash(data)
        return {
            "message": message,
            "data": data,
            "data_hash": data_hash
        }


async def _get(mdb, userid, guildid=None, channelid=None):
    if guildid is not None:
        memberid = struct.pack("!QQ", guildid, userid)
        result = await asyncio.gather(
            mdb.coll_guil.find_one({"_id": Int64(guildid)}),
            mdb.coll_chan.find_one({"_id": Int64(channelid)}),
            mdb.coll_user.find_one({"_id": Int64(userid)}),
            mdb.coll_memb.find_one({"_id": memberid})
            )
        result = [res if res is not None else {} for res in result]
        guild_data, channel_data, user_data, member_data = result
    else:
        user_data = await mdb.coll_user.find_one({"_id": Int64(userid)})
        guild_data = channel_data = member_data = None
    ret = {
        "guild": guild_data,
        "channel": channel_data,
        "user": user_data,
        "member": member_data
    }
    return ret
