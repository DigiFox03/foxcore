import asyncio
import logging
from foxcore.common import dict_hash
from foxcore.pipeline import Block
from .mongodb_client import ClientMongo
log = logging.getLogger(__name__)


class Updater(Block):
    def __init__(self, mdb: ClientMongo):
        self.mdb = mdb

    async def input(self, data, data_hash, **kwargs):
        new_data_hash = dict_hash(data)
        if new_data_hash != data_hash:
            log.debug("updating database")
            await _update(self.mdb, data=data)
        return {
            "data": data,
            "data_hash": new_data_hash
        }


async def _update(mdb, data):
    coros = []
    if data["guild"] is not None:
        guild_data = data["guild"]
        channel_data = data["channel"]
        member_data = data["member"]
        coros.append(mdb.coll_guil.find_one_and_replace(
            {"_id": guild_data["_id"]},
            guild_data,
            upsert=True
        ))
        coros.append(mdb.coll_chan.find_one_and_replace(
            {"_id": channel_data["_id"]},
            channel_data,
            upsert=True
        ))
        coros.append(mdb.coll_memb.find_one_and_replace(
            {"_id": member_data["_id"]},
            member_data,
            upsert=True
        ))
    user_data = data["user"]
    coros.append(mdb.coll_user.find_one_and_replace(
        {"_id": user_data["_id"]},
        user_data,
        upsert=True
    ))
    await asyncio.gather(*coros)
