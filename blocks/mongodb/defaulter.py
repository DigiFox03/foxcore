import struct
import logging
import bson
import discord
from foxcore.common import nickname_dec
from foxcore.pipeline import Block
log = logging.getLogger(__name__)


class Defaulter(Block):
    def __init__(self, mdb):
        self.mdb = mdb

    async def input(self, data, message, **_):
        d_data = {
            "channel": message.channel,
            "user": message.author,
            "member": message.author,
            "guild": message.guild
        }
        data = await defaulter(self.mdb, data=data, d_data=d_data)
        return {
            "data": data
        }


def def_user(user: discord.user):
    dat = {
        "_id": bson.Int64(user.id),
        "name": nickname_dec(user.name),
        "to_greet": False,
        "first_greet": False,
        "permissions": [
            "default"
        ]
    }
    return dat


def def_channel(channel: discord.TextChannel):
    dat = {
        "_id": bson.Int64(channel.id),
        "type": "default",
        "permissions": [
            "default"
        ]
    }
    return dat


def def_guild(guild: discord.Guild):
    dat = {
        "_id": bson.Int64(guild.id)
    }
    return dat


def def_member(member: discord.Member):
    dat = {
        "_id": struct.pack("!QQ", member.guild.id, member.id),
        "name": nickname_dec(member.display_name, member.name),
        "to_greet": False,
        "first_greet": False,
        "permissions": [
            "default"
        ]
    }
    return dat


async def defaulter(mdb, data: dict, d_data: dict):
    if data["user"] is not None:
        default = def_user(d_data["user"])
        default.update(data["user"])
        data["user"] = default
    if data["channel"] is not None:
        default = def_channel(d_data["channel"])
        default.update(data["channel"])
        data["channel"] = default
    if data["guild"] is not None:
        default = def_guild(d_data["guild"])
        default.update(data["guild"])
        data["guild"] = default
    if data["member"] is not None:
        default = def_member(d_data["member"])
        default.update(data["member"])
        data["member"] = default
    return data
