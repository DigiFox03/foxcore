from foxcore.pipeline import Block


class Preprocess(Block):
    def __init__(self, ppsteps):
        self.ppsteps = ppsteps

    async def input(self, **kwargs):
        dt = kwargs
        for step in self.ppsteps:
            dt.update(step.run(**kwargs))
        return dt
